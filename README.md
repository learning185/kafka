# Learning Kafka

This repo is to store the code that was used when learning kafka

## setup project

### install python virtualenv

```
pyenv virtualenv 3.10.5 learn-kafka
pyenv local learn-kafka
pyenv pyright
```
