
CONTAINER_NAME=to_be_set
COMPOSE_LOCAL=docker-compose.yml

## DOCKER COMPOSE
docker:
	docker compose -f $(COMPOSE_LOCAL) up ${ARGS}
docker-d:
	ARGS=-d make docker
docker-build:
	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker compose -f $(COMPOSE_LOCAL) build ${ARGS}
docker-build-clean:
	ARGS=--no-cache make docker-build
docker-stop:
	docker compose -f $(COMPOSE_LOCAL) stop ${ARGS}
##---------------

## DOCKER
exec:
	docker exec -it $(CONTAINER_NAME) bash ${ARGS}
##---------------
