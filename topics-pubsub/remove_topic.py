from confluent_kafka.admin import AdminClient


def main():
    admin_client = AdminClient({"bootstrap.servers": "localhost:9092"})

    topic_list = ["example_topic"]
    results = admin_client.delete_topics(topic_list)
    print(results)
    # Wait for each operation to finish.
    for topic, f in results.items():
        try:
            f.result()  # The result itself is None
            print("Topic {} removed".format(topic))
        except Exception as e:
            print("Failed to remove topic {}: {}".format(topic, e))


if __name__ == "__main__":
    main()
