from confluent_kafka import Consumer
import argparse
import json


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="consumer args")
    parser.add_argument(
        "--topic",
        metavar="T",
        type=str,
        help="topic to consume",
        default="example_topic",
    )
    parser.add_argument(
        "--group",
        metavar="G",
        type=str,
        help="the consumer group",
        default="example_group_1",
    )

    args = parser.parse_args()
    topic = args.topic
    group = args.group

    # Create Consumer instance
    # 'auto.offset.reset=earliest' to start reading from the beginning of the
    #   topic if no committed offsets exist
    consumer_conf = {
        "bootstrap.servers": "localhost:9092",
        "group.id": group,
        "auto.offset.reset": " earliest",
    }
    consumer = Consumer(consumer_conf)

    # Subscribe to topic
    consumer.subscribe([topic])

    # Process messages
    total_count = 0
    try:
        while True:
            msg = consumer.poll(1.0)
            if msg is None:
                # No message available within timeout.
                # Initial message consumption may take up to
                # `session.timeout.ms` for the consumer group to
                # rebalance and start consuming
                print("Waiting for message or event/error in poll()")
                continue
            elif msg.error():
                print("error: {}".format(msg.error()))
            else:
                # Check for Kafka message
                record_key = msg.key()
                record_value = msg.value()
                data = json.loads(record_value)
                count = data["count"]
                total_count += count
                print(
                    "Consumed record with key {} and value {}, \
                      and updated total count to {}".format(
                        record_key, record_value, total_count
                    )
                )
    except KeyboardInterrupt:
        pass
    finally:
        # Leave group and commit final offsets
        consumer.close()
