from confluent_kafka.admin import AdminClient, NewTopic


def main():
    admin_client = AdminClient({"bootstrap.servers": "localhost:9092"})

    topic_list = []
    topic_list.append(NewTopic("example_topic", 1, 1))
    results = admin_client.create_topics(topic_list)
    print(results)
    # Wait for each operation to finish.
    for topic, f in results.items():
        try:
            f.result()  # The result itself is None
            print("Topic {} created".format(topic))
        except Exception as e:
            print("Failed to create topic {}: {}".format(topic, e))


if __name__ == "__main__":
    main()
